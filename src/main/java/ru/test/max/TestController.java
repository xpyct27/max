package ru.test.max;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class TestController {

    @RequestMapping("/test")
    public String test(@RequestParam("param1") String param1) {
        String param2 = "next param";
        return param1 + " " + param2;
    }

}
